#!/bin/bash

WHITE="\e[01;37m"
GREEN="\e[01;32m"
YELLOW="\e[01;33m"
RED="\e[01;31m"
BLUE="\e[01;34m"
RESET="\e[00m"

now()
{
    echo ""
    read -rp " Enter location: " location
    echo ""
    if [ -z $location ]; then
        curl http://wttr.in/?0F
    else
        curl http://wttr.in/~$location?0F
    fi
}

day()
{
    echo ""
    read -rp " Enter location: " location
    echo ""
    if [ -z $location ]; then
        curl http://wttr.in/?1F
    else
        curl http://wttr.in/~$location?1F
    fi
}

full()
{
    echo ""
    read -rp " Enter location: " location
    echo ""
    if [ -z $location ]; then
        curl http://wttr.in/?F
    else
        curl htto://wttr.in/~$location?F
    fi
}

pressanykey()
{
    echo -e " ${YELLOW}\n"
    read -n 1 -s -r -p " Press any key to continue"
}

menu()
{
while true; do
    clear
    echo -e "${BLUE}------------------------------------------${RESET}"
    echo -e "${BLUE}              Weather Report              ${RESET}"
    echo -e "${BLUE}------------------------------------------${RESET}"
    echo -e "${GREEN}------------------------------------------${RESET}"
    echo -e "${GREEN}    Menu:                                 ${RESET}"
    echo -e "${GREEN}------------------------------------------${RESET}"
    echo -e "${YELLOW} 1 ${GREEN}-> ${WHITE}The weather now"
    echo -e "${YELLOW} 2 ${GREEN}-> ${WHITE}Today's weather forecast"
    echo -e "${YELLOW} 3 ${GREEN}-> ${WHITE}Full weather forecast"
    echo -e "${YELLOW} q ${GREEN}-> ${WHITE}Quit${RESET}"
    echo -e "${GREEN}------------------------------------------${RESET}${GREEN}\n"
    read -p " Enter your choice: " menuChoice
    case ${menuChoice} in
        1)
            now
            pressanykey
            ;;
        2)
            day
            pressanykey
            ;;
        3)
            full
            pressanykey
            ;;
        0|q|Q)
            echo
            echo -e "${BLUE} Thanks for using the Weather Report script.${RESET}"
            echo -e "${YELLOW} Powerd by http://wttr.in"
            sleep 1
            exit 0
            ;;
        *)
            echo
            echo -e "${RED} Not an availabe option. ${RESET}"
            echo
            sleep 1
    esac
done
}

menu
