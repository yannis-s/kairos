# kairos

A small weather app using 'http://wttr.in'.

---

#### Dependancies

In oreder to run kairos.sh, `curl` needs to be present
    
* Debian, Ubnuntu and other Debian-like derivatives
    
    `sudo apt install curl`

* openSUSE:
    
    `sudo zypper install curl`

* CentOS, Fedora
    
    `sudo yum install curl`

---

After you download the script:

* make it executable:

`chmod +x kairos.sh`

* if you want to be executable system wide (and for users), then move kairos to /usr/local/bin:

`sudo mv kairos.sh /usr/local/bin`

Now you can use this script like any other terminal program.

---

#### How to use it:

There are 3 options the first gives the current weather, the second one shows all days's current forecast and the third options gives a three days forecast.
Will prompt you to enter your desired location.

e.x.:

```
------------------------------------------
              Weather Report
------------------------------------------
------------------------------------------
    Menu:
------------------------------------------
 1 -> The weather now
 2 -> Today's weather forecast
 3 -> Full weather forecast
 q -> Quit
------------------------------------------
 Enter your choice:
```

```
------------------------------------------
              Weather Report
------------------------------------------
------------------------------------------
    Menu:
------------------------------------------
 1 -> The weather now
 2 -> Today's weather forecast
 3 -> Full weather forecast
 q -> Quit
------------------------------------------
 Enter your choice: 1

 Enter location: athens_greece

Weather report: athens_greece

       .-.      Rain
      (   ).    19 °C
     (___(__)   ↓ 0 km/h
    ‚‘‚‘‚‘‚‘    10 km
    ‚’‚’‚’‚’    0.4 mm


 Press any key to continue
```

---

Feel free  to download use or modify to your liking.
